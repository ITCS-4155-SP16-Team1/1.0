/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author David
 */
public class Organizer {
        private String oId;
    private Date oDate;

    public Organizer(String oId, Date oDate) {
        this.oId = oId;
        this.oDate = oDate;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public void setlDate(Date oDate) {
        this.oDate = oDate;
    }

    public String getoId() {
        return oId;
    }

    public Date getoDate() {
        return oDate;
    }
    
}
