/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author David
 */
public class Outreach extends Event{
    private Lead outreachLead;

    public Outreach(Lead outreachLead, String eventTitle, Date eventDate, String eventBegin, String eventEnd, String eventLocation, String eventAddress) {
       
        this.outreachLead = outreachLead;
    }

    public Outreach(Lead outreachLead) {
        this.outreachLead = outreachLead;
    }

    public Outreach(String eventTitle, Date eventDate, String eventBegin, String eventEnd, String eventLocation, String eventAddress) {

    }

    public Outreach() {
    }

    public void setOutreachLead(Lead outreachLead) {
        this.outreachLead = outreachLead;
    }

    public Lead getOutreachLead() {
        return outreachLead;
    }
    
}
