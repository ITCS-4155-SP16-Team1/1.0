<%-- 
    Document   : registration
    Created on : Mar 21, 2016, 2:00:33 AM
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Member</title>
            <link rel="stylesheet" type="text/css" href="css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="css/foundation.css">
   
    <link rel="stylesheet" type="text/css" href="css/app.css">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.css" rel="stylesheet" media="screen">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.min.css" rel="stylesheet"> 
        <link type="text/css" href="styles/mainStyle.css" rel="stylesheet">
        <link type="text/css" href="styles/full-slider.css" rel="stylesheet">
       <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> 
     
    </head>
    <body>
         <%@ include file="header.jsp" %> 
      <div id="wrapper" class="container center-block">
        <div id="RegInfo" class="row">
        <h1>Registration</h1>
       
                      
                <form action="MemberShip" method="post" class="panel panel-primary col-lg-4 col-lg-offset-4">
                    <input type="hidden" name="action" value="signup">
                    
                    <div class="input-group input-group-md">
                     <input type="text" name="lname" class="form-control" placeholder="Last Name">
                    </div>
                    <div class="input-group input-group-md">                                      
                   
                    <input type="text" name="fname" class="form-control" placeholder="First Name">
                    </div>
                     <div class="input-group input-group-md">
                   
                    <select name="occp" id="occup" value="Student">
                                <option>Student</option>
                                <option>Instructor</option>
                     </select>
                    </div>
                    <div class="input-group input-group-md">
                    <span class="input-group-addon">
                   <span class="glyphicon glyphicon-comment"></span>
                   </span>
                    <textarea   name="address" class="form-control" rows="5" placeholder="Address"></textarea>
                    </div>
                     <div class="input-group input-group-md">
                    <span class="input-group-addon">
                   <span class="glyphicon glyphicon-phone"></span>
                    </span>
                    <input type="text" name="phone" class="form-control" placeholder="(000)0000000">
                    </div>
                    <div class="input-group input-group-md">
                    <span class="input-group-addon">@</span>
                    
                    <input type="text" name="email" class="form-control" placeholder="email">
                    </div>
                    <div class="input-group input-group-md">
                    <span class="input-group-addon">
                   <span class="glyphicon glyphicon-lock"></span>
                   </span>
                    <input type="password" name="psw" class="form-control" placeholder="Password">
                    </div>
                    <br>
                    <input type="submit"  class=" btn btn-primary btn-block" value="Sign up">
                       
                </form>
            
     
        </div>
        <div id="systemInfo" class="row">
        
        </div>
        
    </div>
        
            <script src="sripts/jquery-1.11.3.js"></script>
<script src="js/foundation.js"></script>
<script src="js/foundation.min.js"></script>
<script src="sripts/firstScript.js"> </script>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.orbit.js"></script>
       <script 	src="bootstrapScripts/js/bootstrap.min.js"></script>

    </body>
</html>
