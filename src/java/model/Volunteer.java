/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author David
 */
public class Volunteer  extends Member{
    private String vId;
    private Date vDate;

    public Volunteer(String memberName, String memberFirst, String memberEmail, String memberPassword, String memberAddress, String memberPhone, String memberOccupation, Date memberRegDate) {
        super(memberName, memberFirst, memberEmail, memberPassword, memberAddress, memberPhone, memberOccupation, memberRegDate);
    }

    public Volunteer() {
        super();
    }

    public void setvId(String vId) {
        this.vId = vId;
    }

    public void setvDate(Date vDate) {
        this.vDate = vDate;
    }

    public String getvId() {
        return vId;
    }

    public Date getvDate() {
        return vDate;
    }
    
}
