/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.EventDB;
import static controllers.EventDB.insertEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.console;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Event;
import model.Member;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author David
 */
@WebServlet("/Activities")
public class Activities extends HttpServlet {

  

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
      doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // String.format("%04d", ((int)(Math.random()*9999)))
         String action=request.getParameter("action");
         HttpSession session = request.getSession();
       Member member = (Member) session.getAttribute("member");
             if(action.equals("showEvents")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
             
            ArrayList<String> events= EventDB.getAllEvents();
            
            String eventsToString=  "{\"events\": "+ events.toString()+ " }";
          
            

                  JSONArray  eList = new JSONArray();
                
                  eList.put(eventsToString);
                 out.println(eList);
              
              
                
           
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    } else if(action.equals("showCourses")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
             
            ArrayList<String> courses= EventDB.getAllCourses();
            
            String coursesToString=  "{\"courses\": "+ courses.toString()+ " }";
          
            

                  JSONArray  cList = new JSONArray();
                
                  cList.put(coursesToString);
                 out.println(cList);
              
         
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
     else if(action.equals("showOutreaches")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
             
            ArrayList<String> outreaches= EventDB.getAllOutReaches();
            
            String outreachesToString=  "{\"outreaches\": "+ outreaches.toString()+ " }";
          
            

                  JSONArray  outList = new JSONArray();
                
                  outList.put(outreachesToString);
                 out.println(outList);
              
              
          
           
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
             
else if(action.equals("insertEvent")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
               FileWriter output;
         output = new FileWriter("\\mydata.txt",true);    
         
            
             
                   String eId=request.getParameter("eventid");  
                   output.write(eId);
                   //output.write(eId);    
                   String mId = member.getMemberID();
                   output.write(mId);
               output.close(); 
                  // String mId 
                   //output.write(mId); 
                           //request.getParameter("enumber");
                                   
        
      
                     

         
                EventDB.insertEvent( mId, eId);
          
            

                  String  msg = "Event added";
              
                 out.println(msg);
              
              
          
           
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
  else if(action.equals("insertEvent")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
               FileWriter output;
         output = new FileWriter("\\mydata.txt",true);    
         
            
             
                   String eId=request.getParameter("eventid");  
                   output.write(eId);
                   //output.write(eId);    
                   String mId = member.getMemberID();
                   output.write(mId);
               output.close(); 
                  // String mId 
                   //output.write(mId); 
                           //request.getParameter("enumber");
                                   
        
      
                     

         
                EventDB.insertCourse( mId, eId);
          
            

                  String  msg = "Event added";
              
                 out.println(msg);
              
              
          
           
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
             else if(action.equals("insertOutreach")) { 
          response.setContentType("application/json;charset=utf-8");
            PrintWriter out= response.getWriter();
          try {
            
         
            
             
                   String eId=request.getParameter("eventid");  
                  
                   String mId = member.getMemberID();
                 
                                   
        
      
                     

         
                EventDB.insertOutreach( mId, eId);
          
            

                  String  msg = "Event added";
              
                 out.println(msg);
              
              
          
           
        } catch (SQLException ex) {
            Logger.getLogger(Activities.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    }

}
