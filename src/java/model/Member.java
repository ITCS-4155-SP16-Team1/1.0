/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.*;

/**
 *
 * @author David
 */
public class Member {
   private String memberID;
    private String memberName;
    private String memberFirst;
    private String memberEmail;
    private String memberAddress;
    private String memberPhone;
    private String memberOccupation;
    private Date memberRegDate;
    private String memberPassword;

    public Member(String memberName, String memberFirst, String memberEmail, String memberPassword,String memberAddress, String memberPhone, String memberOccupation, Date memberRegDate) {
       
        this.memberName = memberName;
        this.memberFirst = memberFirst;
        this.memberEmail = memberEmail;
        this.memberPassword= memberPassword;
        this.memberAddress = memberAddress;
        this.memberPhone = memberPhone;
        this.memberOccupation = memberOccupation;
        this.memberRegDate = memberRegDate;
    }

    public Member() {
          this.memberName = "";
        this.memberFirst = "";
        this.memberEmail = "";
        this.memberPassword= "";
        this.memberAddress = "";
        this.memberPhone = "";
        this.memberOccupation = "";
        this.memberRegDate = new Date();
     }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }


    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public void setMemberFirst(String memberFirst) {
        this.memberFirst = memberFirst;
    }

    public String getMemberPassword() {
        return memberPassword;
    }

    public void setMemberPassword(String memberPassword) {
        this.memberPassword = memberPassword;
    }

    public void setMemberEmail(String memverEmail) {
        this.memberEmail = memverEmail;
    }

    public void setMemberAddress(String memberAddress) {
        this.memberAddress = memberAddress;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public void setMemberOccupation(String memberOccupation) {
        this.memberOccupation = memberOccupation;
    }

    public void setMemberRegDate(Date memberRegDate) {
        this.memberRegDate = memberRegDate;
    }

    public String getMemberID() {
        return memberID;
    }



    public String getMemberName() {
        return memberName;
    }

    public String getMemberFirst() {
        return memberFirst;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public String getMemberAddress() {
        return memberAddress;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public String getMemberOccupation() {
        return memberOccupation;
    }

    public Date getMemberRegDate() {
        return memberRegDate;
    }
    
}
