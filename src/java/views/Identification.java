/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.EventDB;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controllers.MemberDB;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import model.Member;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import java.lang.System;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import org.json.JSONArray;
//For debugging



/**
 *
 * @author David
 */

public class Identification extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        
       String url = "/index.html";
       	//Grab user session from site
    	HttpSession session = request.getSession();
    	Member member = (Member) session.getAttribute("member");
        PrintWriter out = response.getWriter();
        // get current action
        String action = request.getParameter("action");
        if (session==null)
        {
            session = request.getSession(true);
            action="returning";

        }
        else
        {
            if (member!=null)
        {   
            action="returning";
            
        }
            
        }	//get action request sent from client side
		
     if (action == null) {
            action = "standard";  // default action
          
        }

        // perform action and set URL to appropriate page
        if (action.equals("steandard")) {
            url = "/index.jsp";    // the "join" page
        }
        else if (action.equals("logout")) {
            url = "/index.jsp";    // the "join" page
             session.invalidate();
        }
                else if (action.equals("returning")) {  
      
           try {
               Cookie [] ck = request.getCookies();
           
                String theUserName = ck[0].getName();
                String psw =ck[1].getName();
                
                member=MemberDB.account(theUserName,psw);
               
               
                    if(member!=null)  
                   
               { 
              
                   session.setAttribute("member", member);
                  
                    url = "/calendar.jsp";
               }
              else {
                        
                   url = "/index.jsp"; 
               }
            
                } catch (SQLException ex) {
                 Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
             }  
           
        }
        else if (action.equals("login")) {  
      
           try {
               
                String theUserName = request.getParameter("email");
                String psw = request.getParameter("psw");
                             
                 member = MemberDB.account(theUserName,psw);
               
                    if(member!=null)  
                   
               { 
              
                   session.setAttribute("member", member);
                     
        
        Cookie eaddr = new Cookie("email",theUserName);
        eaddr.setMaxAge(1800);
        Cookie pssw = new Cookie("passw",psw);
        pssw.setMaxAge(1800);
        response.addCookie(eaddr);
        response.addCookie(pssw);
                    url = "/index.jsp";
                    
         //            String role="{\"menu\": [{\"role\":\""+ member.getMemberOccupation()+"\"}]}";
         //   FileWriter output;
        
    //  output = new FileWriter("\\mydata.txt",true);    
         
        //   output.write(role);     
          
          // output.close(); 
              //    JSONArray  menurole = new JSONArray();
                
               //   menurole.put(role);
              //  out.println(menurole);
              
                   
               }
              else {
                        String error="User not found";
                   request.setAttribute("errorMsg", error);    
                   
                   url = "/login.jsp"; 
               }
            
                } catch (SQLException ex) {
                 Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
             }  
           
        }
   
          else if (action.equals("signup")) {                
            try {
                // get parameters from the request
               String lastName = request.getParameter("lname");
               String firstName = request.getParameter("fname");
               String email = request.getParameter("email");
               String passw = request.getParameter("psw");
               String occupation = request.getParameter("occp");
               String address = request.getParameter("address");
               String phone = request.getParameter("phone"); 
               Date rdate= new Date();
                 member = new Member(lastName,firstName,email,passw,address, phone, occupation, rdate);
                
               MemberDB.insert(member);
                
                // set Member object in request object and set URL
                session.setAttribute("member", member);
                
                //Send email
                //try
                //{
                    // store the User object in the session
      
        session.setAttribute ("member",member) ;
        
        Cookie eaddr = new Cookie("email",email);
        eaddr.setMaxAge(1800);
        Cookie psw = new Cookie("passw",passw);
        psw.setMaxAge(1800);
        response.addCookie(eaddr);
        response.addCookie(psw);
        url = "/thanks.jsp" ;  
    //send email to user
String to = email;
String from = "dekoue@uncc.edu" ;
String subject = "welcome";
String body ="Dear " + firstName + ", \n\n" +
"Thanks for joining our Organization." ;
boolean isBodyHTML = false;
                                     
             
                   try{                                              
                    MailUtilLocal.sendMail(to, from, subject, body, isBodyHTML) ;
                    
                    }
                catch (MessagingException e)
                
                     
                    {
                   
                String errorMessage =
"ERROR: Unable to sand email . " +
"Check Tomcat logs for datails . <br>" +
"NOTE : You may nEEd to configure your system " +
" <br>" +
"EERROR MESSAGE : " + e.getMessage ( ) ;
request . setAttribute ( "errorMessage" , errorMessage) ;
this . log("unable to sand email . \n" +
"Here is the email you triad to send: \n" +
"=====================================\n" +
"TO: " + email + "\n" +
"FROM: " +from + " \n" +
"SUBJBCT : " + subject + "\n" +
"\n" +
body + "\n\n ") ; 

 }
                   
//forward request and response to JSP page

 
   
        }  catch (SQLException ex) {
               Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
           }
         
       
      }
        // forward request and response objects to specified URL
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response); 
        
    }


}
