/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author David
 */
public class Lead extends Volunteer{
        private String lId;
    private Date lDate;

    public Lead(String lId, Date lDate) {
        this.lId = lId;
        this.lDate = lDate;
    }

  

    public Lead() {
    }

    public void setlId(String lId) {
        this.lId = lId;
    }

    public void setlDate(Date lDate) {
        this.lDate = lDate;
    }

    public String getlId() {
        return lId;
    }

    public Date getlDate() {
        return lDate;
    }
    
}
