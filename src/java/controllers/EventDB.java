/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import model.Member;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import model.Event;
/**
 *
 * @author David
 */
public class EventDB {
          private static Statement statement = null;
	  private static ResultSet resultSet = null;
 public void add(Event e)       
 {
     
 }  
 
 public Event get(int enumber)       
 {
     Event e= new Event();
     
     return e;
 }  
 
  public static ArrayList<String> getAllEvents() throws SQLException, IOException       
 {
    
     resultSet=null ;
   
     String ev="" ;
     ArrayList<String> lev= new ArrayList<String>();
if(ConnectionDB.open()!= null){
       System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
    String sql = "SELECT* FROM event"; 
      resultSet= statement.executeQuery(sql);
   //    FileWriter output;
        
       //output = new FileWriter("\\mydata.txt",true);    
         
       while(resultSet.next())
       {
      
     
         
             
         ev="{\"eventID\":\""+resultSet.getString("enumber")+
           "\",\"eventTitle\":\""+resultSet.getString("etitle")+
            "\",\"eventDesc\":\""+resultSet.getString("edescript")+
             "\",\"eventDate\":\""+resultSet.getDate("edate")+
            "\",\"eventBegin\":\""+resultSet.getString("ebegintime")+
           "\",\"eventEnd\":\""+resultSet.getString("endtime")+
           "\",\"eventLocation\":\""+resultSet.getString("elocatioon")+
           "\",\"eventAddress\":\""+resultSet.getString("eaddress")+
            "\",\"eventDayText\":\""+dayOf(resultSet.getDate("edate").getDay())+
            "\",\"eventMonText\":\""+monthOf(resultSet.getDate("edate").getMonth())+       
            "\",\"eventDay\":\""+String.format("%02d",resultSet.getDate("edate").getDate())+
          "\",\"eventMon\":\""+String.format("%02d",resultSet.getDate("edate").getMonth())+
           "\",\"eventYear\":\""+String.format("%04d",resultSet.getDate("edate").getYear())+
                 "\"}" ;
               
          
             lev.add(ev); 
           //output.write(ev);     
          
          // output.close(); 
       }
      
     ConnectionDB.close();
}

     return lev;
 }  

  
public static ArrayList<String> getAllCourses() throws SQLException, IOException       
 {
    
     resultSet=null ;
   
     String co="" ;
     ArrayList<String> lco= new ArrayList<String>();
if(ConnectionDB.open()!= null){
       System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
    String sql = "SELECT* FROM scheduled_courses"; 
      resultSet= statement.executeQuery(sql);
   //    FileWriter output;
          //output = new FileWriter("\\mydata.txt",true);
          
         
       while(resultSet.next())
       {
      
    
         
             
         co="{\"eventID\":\""+resultSet.getString("cnumber")+
           "\",\"eventTitle\":\""+resultSet.getString("ctitle")+
            "\",\"eventDesc\":\""+resultSet.getString("cdescript")+
             "\",\"eventDate\":\""+resultSet.getDate("cdate")+
            "\",\"eventBegin\":\""+resultSet.getString("cbegintime")+
           "\",\"eventEnd\":\""+resultSet.getString("cndtime")+
           "\",\"eventLocation\":\""+resultSet.getString("clocatioon")+
           "\",\"eventAddress\":\""+resultSet.getString("caddress")+
            "\",\"eventDayText\":\""+dayOf(resultSet.getDate("cdate").getDay())+
            "\",\"eventMonText\":\""+monthOf(resultSet.getDate("cdate").getMonth())+       
            "\",\"eventDay\":\""+String.format("%02d",resultSet.getDate("cdate").getDate())+
          "\",\"eventMon\":\""+String.format("%02d",resultSet.getDate("cdate").getMonth())+
           "\",\"eventYear\":\""+String.format("%04d",resultSet.getDate("cdate").getYear())+
            "\",\"eventInsLastName\":\""+resultSet.getString("lName")+
            "\",\"eventInsFirstName\":\""+resultSet.getString("fName")+     
                 "\"}" ;
               
          
             lco.add(co); 
           //output.write(ev);     
          
          // output.close(); 
       }
      
     ConnectionDB.close();
}

     return lco;
 }  
    
public static ArrayList<String> getAllOutReaches() throws SQLException, IOException       
 {
    
     resultSet=null ;
   
     String outr="" ;
     ArrayList<String> loutr= new ArrayList<String>();
if(ConnectionDB.open()!= null){
       System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
    String sql = "SELECT* FROM scheduled_outreaches"; 
      resultSet= statement.executeQuery(sql);
   //    FileWriter output;
        
          
         
       while(resultSet.next())
       {
      
      //output = new FileWriter("\\mydata.txt",true);
         
             
         outr="{\"eventID\":\""+resultSet.getString("outnumber")+
           "\",\"eventTitle\":\""+resultSet.getString("outtitle")+
            "\",\"eventDesc\":\""+resultSet.getString("outdescript")+
             "\",\"eventDate\":\""+resultSet.getDate("outdate")+
            "\",\"eventBegin\":\""+resultSet.getString("outbegintime")+
           "\",\"eventEnd\":\""+resultSet.getString("outndtime")+
           "\",\"eventLocation\":\""+resultSet.getString("outlocatioon")+
           "\",\"eventAddress\":\""+resultSet.getString("outaddress")+
            "\",\"eventDayText\":\""+dayOf(resultSet.getDate("outdate").getDay())+
            "\",\"eventMonText\":\""+monthOf(resultSet.getDate("outdate").getMonth())+       
            "\",\"eventDay\":\""+String.format("%02d",resultSet.getDate("outdate").getDate())+
          "\",\"eventMon\":\""+String.format("%02d",resultSet.getDate("outdate").getMonth())+
           "\",\"eventYear\":\""+String.format("%04d",resultSet.getDate("outdate").getYear())+
            "\",\"eventLeadLastName\":\""+resultSet.getString("lName")+
            "\",\"eventLeadFirstName\":\""+resultSet.getString("fName")+     
                 "\"}" ;
               
          
             loutr.add(outr); 
           //output.write(ev);     
          
          // output.close(); 
       }
      
     ConnectionDB.close();
}

     return loutr;
 }  
 
  public static int getEventKey() throws SQLException       
 {
     int eKey=0;
     resultSet=null ;
 
     
if(ConnectionDB.open()!= null){
       System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
    String sql = "SELECT event FROM parameters"; 
      resultSet= statement.executeQuery(sql);
       while(resultSet.next())
       {
           eKey= resultSet.getInt("event");
        
       }
      
     ConnectionDB.close();
}
     return eKey;
 }
  
   public static void insertEvent(String mId, String eId) throws SQLException, FileNotFoundException, IOException {
    
if(ConnectionDB.open() != null){
    
      System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.getStatement();
    // java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
  FileWriter output;
         output = new FileWriter("\\mydata.txt",true);
               output.write(mId);     
          output.write(eId);  
          output.close(); 
     // String currentdate = sdf.format(member.getMemberRegDate());
     String sql = "INSERT INTO event_attend (enumber,id_member) VALUES"+
             "('"+eId+"','"+mId+"')"; 
        
     statement.execute(sql);
     System.out.println("Inserted records into the table...");
     ConnectionDB.close();
}
    
 }
     public static void insertOutreach(String mId, String oId) throws SQLException, FileNotFoundException, IOException {
    
if(ConnectionDB.open() != null){
    
      System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.getStatement();
    // java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

     // String currentdate = sdf.format(member.getMemberRegDate());
     String sql = "INSERT INTO outreach_attend (id_member,outnumber) VALUES"+
             "('"+mId+"','"+oId+"')"; 
              
     statement.execute(sql);
     System.out.println("Inserted records into the table...");
     ConnectionDB.close();
}
    
 }
     
      public static void insertCourse(String mId, String cId) throws SQLException, FileNotFoundException, IOException {
    
if(ConnectionDB.open() != null){
    
      System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.getStatement();
    // java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

     // String currentdate = sdf.format(member.getMemberRegDate());
     String sql = "INSERT INTO course_attend (id_member,cnumber) VALUES"+
             "('"+mId+"','"+cId+"')"; 
              
     statement.execute(sql);
     System.out.println("Inserted records into the table...");
     ConnectionDB.close();
}
    
 }   
      
public static ArrayList<String> getSchedules() throws SQLException, IOException       
 {
    
     resultSet=null ;
   
     String sc="" ;
     ArrayList<String> lsc= new ArrayList<String>();
if(ConnectionDB.open()!= null){
       System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
    String sql = "SELECT* FROM allevents"; 
      resultSet= statement.executeQuery(sql);
   //    FileWriter output;
        
          
         
       while(resultSet.next())
       {
      
      //output = new FileWriter("\\mydata.txt",true);
         
             
         sc="{\"id\":\""+resultSet.getString("cnumber")+
           "\",\"title\":\""+resultSet.getString("ctitle")+
           
             "\",\"start\":\""+resultSet.getDate("cdate")+"T"+resultSet.getTime("cbegintime")+
              "\",\"end\":\""+resultSet.getDate("cdate")+"T"+resultSet.getTime("cndtime")+
             
                 "\"}" ;
               
          
             lsc.add(sc); 
           //output.write(ev);     
          
          // output.close(); 
       }
      
     ConnectionDB.close();
}

     return lsc;
 }  
 
    
      
      
           private static String monthOf(int value)
       { String theMonth="";
       switch(value)
       {
           case 1:
           {
               theMonth="Jan";
               break;
           }
             case 2:
           {
               theMonth="Feb";
               break;
           }
            case 3:
           {
               theMonth="Mar";
               break;
           }
              case 4:
           {
               theMonth="Apr";
               break;
           }
              
             case 5:
           {
               theMonth="May";
               break;
           }
               case 6:
           {
               theMonth="Jun";
               break;
           }
                 case 7:
           {
               theMonth="Jul";
               break;
           }
                case 8:
           {
               theMonth="Aug";
               break;
           }
               case 9:
           {
               theMonth="Sep";
               break;
           }
                 case 10:
           {
               theMonth="Oct";
               break;
           }              
                          case 11:
           {
               theMonth="Nov";
               break;
           }
                 case 12:
           {
               theMonth="Dec";
               break;
           }       
       }
           return theMonth;
       
       }
            private static String dayOf(int value)
       { String theDay="";
       switch(value)
       {
           case 1:
           {
               theDay="Mon";
               break;
           }
             case 2:
           {
               theDay="Tue";
               break;
           }
            case 3:
           {
               theDay="Wed";
               break;
           }
              case 4:
           {
               theDay="Thu";
               break;
           }
              
             case 5:
           {
               theDay="Fri";
               break;
           }
               case 6:
           {
               theDay="Sat";
               break;
           }
                 case 0:
           {
               theDay="Sun";
               break;
           }
       }
           return theDay;
       
       }

}
