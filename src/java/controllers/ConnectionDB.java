/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author David
 */
public class ConnectionDB {

	  public static String url ="jdbc:mysql://localhost:3306/stars"; 
          public static String admin ="root";
          public static String adminPwd ="";
          public static Connection conn ;
          private static Statement statement = null;
 public static Connection open() throws SQLException
 {
            System.out.println("Connecting to a selected database...");
       DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        
         conn = DriverManager.getConnection(url, admin, adminPwd);
       
     return conn;
 
 }         

    public static Statement getStatement() throws SQLException {
         statement=conn.createStatement();
        return statement;
    }
 
 public static void close() throws SQLException
 {
  conn.close();
}
}