<!DOCTYPE html5>
<html>
<head>
    <title>Welcome to StarsMag</title>
    
    <link rel="stylesheet" type="text/css" href="css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="css/foundation.css">
   
    <link rel="stylesheet" type="text/css" href="css/app.css">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.css" rel="stylesheet" media="screen">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.min.css" rel="stylesheet"> 
        <link type="text/css" href="styles/mainStyle.css" rel="stylesheet">
        <link type="text/css" href="styles/full-slider.css" rel="stylesheet">
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> 
        <link type="text/css" href="css/calendar.css" rel="stylesheet">
        <link type="text/css" href='styles/fullcalendar.css' rel='stylesheet' />
        <link type="text/css" href='styles/fullcalendar.print.css' rel='stylesheet' media='print' />


</head>
<body>

       <%@ include file="header.jsp" %> 
       <br>
       <div id="calContainer" class="row">    
    <div id="calendar"></div>
       </div>
        <!-- Footer -->
    <footer>
  
 
        
    </footer>
    
<script src="sripts/jquery-1.11.3.js"></script>
<script src="js/foundation.js"></script>
<script src="js/foundation.min.js"></script>
<script src="sripts/firstScript.js"> </script>

<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.orbit.js"></script>
<script src="bootstrapScripts/js/bootstrap.min.js"></script>
<script src='sripts/moment.min.js'></script>
<script src='sripts/jquery.min.js'></script>
<script src='js/fullcalendar.min.js'></script>

<script src="sripts/calendarScript.js"> </script>
</body>
</html>