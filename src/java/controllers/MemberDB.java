/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;


import java.io.IOException;
import model.Member;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
/**
 *
 * @author David
 */

public class MemberDB {
    
       	  private static Statement statement = null;
	  private static ResultSet resultSet = null;

    
        public static Member account(String username, String psw) throws SQLException {
 
     resultSet=null ;
     Member sessionUser = new Member();
     
if(ConnectionDB.open() != null){
    
      System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.open().createStatement();
     
     String sql = "SELECT* FROM member WHERE email='"+username+"' AND passw='"+psw+"'"; 
      resultSet= statement.executeQuery(sql);
      if(resultSet.next())
       {    sessionUser.setMemberID(resultSet.getString("id"));    
           sessionUser.setMemberName(resultSet.getString("lName"));
           sessionUser.setMemberFirst(resultSet.getString("fName"));
           sessionUser.setMemberEmail(resultSet.getString("email"));
            sessionUser.setMemberAddress(resultSet.getString("address"));
           sessionUser.setMemberPassword(resultSet.getString("passw"));
           sessionUser.setMemberOccupation(resultSet.getString("occupation"));
           sessionUser.setMemberPhone(resultSet.getString("phone"));
           sessionUser.setMemberRegDate(resultSet.getDate("firstDate"));
       }
      else sessionUser=null;
     ConnectionDB.close();
}
    
    return sessionUser;   
    }
        
        
        public static long insert(Member member) throws SQLException, FileNotFoundException, IOException {
    
if(ConnectionDB.open() != null){
    
      System.out.println("Connected database successfully...");
      
      statement=ConnectionDB.getStatement();
     java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

      String currentdate = sdf.format(member.getMemberRegDate());
     String sql = "INSERT INTO member (lName,fName,email,passw,address,phone,occupation,firstDate) VALUES"+
             "('"+member.getMemberName()+"','"+member.getMemberFirst()+"','"+member.getMemberEmail()+"','"+
             member.getMemberPassword()+"','"+member.getMemberAddress()+"','"+member.getMemberPhone()+"','"+member.getMemberOccupation()+"','"+
             currentdate+"')"; 
     

         
     statement.execute(sql);
     System.out.println("Inserted records into the table...");
     ConnectionDB.close();
}
    
        return 0;
 }
}
