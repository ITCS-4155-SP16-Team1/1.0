<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<DOCTYPE html5>
<html>
<head>
    <title>Welcome to StarsMag</title>
    
    <link rel="stylesheet" type="text/css" href="css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="css/foundation.css">
    <link rel="stylesheet" type="text/css" href="css/app.css">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.css" rel="stylesheet" media="screen">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.css" rel="stylesheet">
        <link type="text/css" href="bootstrapScripts/css/bootstrap.vertical-tabs.min.css" rel="stylesheet"> 
        <link type="text/css" href="styles/mainStyle.css" rel="stylesheet">
         <link type="text/css" href="styles/stars.css" rel="stylesheet">
        <link type="text/css" href="styles/full-slider.css" rel="stylesheet">
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> 
        <link type="text/css" href="css/events.css" rel="stylesheet">      
</head>
<body>
        <!-- Navigation -->
     <%@ include file="header.jsp" %> 
     
    
      <div id="menu">
          <br>
          <br>
              <ul class="nav nav-pills nav-justified" data-toggle="tab">
                  <li class="active">
                        <a href="#tab1" id="tOutreaches" data-toggle="pill"  data-toggle="tab">Outreach</a>
                    </li>
                    <li>
                        <a href="#tab2" id="tCourses" data-toggle="pill" data-toggle="tab">Course</a>
                    </li>
                     <li>
                         <a href="#tab3" id="tEvents" data-toggle="pill" data-toggle="tab"> Events</a>
                    </li>
          </ul>
      </div>
   
          <div id="activities" class="tab-content">
          <div  id="tab1" class="tab-pane active">
	   <div class="small-9 columns small-centered">

    

    <article class="event">

        <div class="event-date">
          <p class="event-month">Month</p>
          <p class="event-day">Day</p>
          <p class="event-month">Time</p>
        </div>

        <div class="event-desc">
          <h4 class="event-desc-header">Outreach</h4>
          <p class="event-desc-detail"><span class="event-desc-time"></span>Description</p>
          <a href="" class="rsvp button">Link to Documentation</a>
        </div>
         <div class="event-date">
          <p class="event-month">Instructor</p>
          <p class="event-day">Name</p>
        </div>
         
      </article>

      <hr>


  </div>
             
                   
                      
	   </div>
	    <div id="tab2" class="tab-pane">
	     
                <article class="event">

        <div class="event-date">
          <p class="event-month">Month</p>
          <p class="event-day">Day</p>
        </div>

        <div class="event-desc">
          <h4 class="event-desc-header">Course</h4>
          <p class="event-desc-detail"><span class="event-desc-time"></span>Description</p>
          <a href="" class="rsvp button">Link to Documentation</a>
        </div>

      </article>
                    <hr>
                
                
	    </div>
		<div id="tab3" class="tab-pane">
                    
	
    	
      <article class="event">

        <div class="event-date">
          <p class="event-month"></p>
          <p class="event-day"></p>
        </div>

        <div class="event-desc">
          <h4 class="event-desc-header"></h4>
          <p class="event-desc-detail"><span class="event-desc-time"></span></p>
          <a href="" class="rsvp button" name="">Subscribe</a>
        </div>

      </article>
                    <hr>
                    
             
	    </div>
		
	                    
	</div>
    
    
        
    
<nav id="social" class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
         <div class="icon-bar">
       <a href="https://www.facebook.com/groups/unccstars/?fref=ts"><i class="fa fa-facebook" style="font-size:24px;"></i></a>
       <a href="https://twitter.com/STARSCorps"><i class="fa fa-twitter" style="font-size:24px;"></i></a>
       <a href="http://linkedin.com/"><i class="fa fa-linkedin" style="font-size:24px;"></i></a>
      <a href="http://uncc.starscomputingcorps.org/contact"><i class="fa fa-envelope" style="font-size:24px;"></i>   </a> 
    
    

         </div>
    </nav>
    
</body>  

<script src="sripts/jquery-1.11.3.js"></script>
<script src="js/foundation.js"></script>
<script src="js/foundation.min.js"></script>
<script src="sripts/firstScript.js"> </script>
<script src="sripts/eventScript.js"> </script>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.orbit.js"></script>
 <script src="bootstrapScripts/js/bootstrap.min.js"></script>

</html>