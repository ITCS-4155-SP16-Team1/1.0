<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Email sent</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>    
</head>

<body>
    <h1>Thanks for joining Stars Organization</h1>

    <p>Here is the information that you entered:</p>

    <label>Email:</label>
    <span>${member.memberEmail}</span><br>


</body>
</html>