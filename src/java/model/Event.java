/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Time;
import java.util.Date;
/**
 *
 * @author David
 */
public class Event {
    private String eventID;
    private String eventTitle;
    private String eventDesc;
    private Date eventDate;
    private String eventBegin;
    private String eventEnd;
    private String eventLocation;
    private String eventAddress;
    private String eventDay;
    private String eventMon;
    private String eventDayText;
    private String eventMonText;
    private String  eventYear;
    public Event(String eventTitle, String eventDesc, Date eventDate, String eventBegin, String eventEnd, String eventLocation, String eventAddress) {
        this.eventTitle = eventTitle;
        this.eventDesc= eventDesc;
        this.eventDate = eventDate;
        this.eventBegin = eventBegin;
        this.eventEnd = eventEnd;
        this.eventLocation = eventLocation;
        this.eventAddress = eventAddress;
    }
   
       public Event() {
        this.eventTitle = "";
        this.eventDesc= "";
        this.eventDate = new Date();
        this.eventBegin = "00:00:00";
        this.eventEnd = "00:00:00";
        this.eventLocation = "";
        this.eventAddress = "";
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }
    
    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
        
        eventDay= String.format("%02d", this.eventDate.getDate());
        eventDayText=dayOf(this.eventDate.getDay());
        eventMon=String.format("%02d",this.eventDate.getMonth());
        eventMonText=monthOf(this.eventDate.getMonth());
        eventYear=String.format("%04d",this.eventDate.getYear());
    }

    public void setEventBegin(String eventBegin) {
        this.eventBegin = eventBegin;
    }

    public void setEventEnd(String eventEnd) {
        this.eventEnd = eventEnd;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public void setEventAddress(String eventAddress) {
        this.eventAddress = eventAddress;
    }

    public String getEventID() {
        return eventID;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public String getEventBegin() {
        return eventBegin;
    }

    public String getEventEnd() {
        return eventEnd;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public String getEventAddress() {
        return eventAddress;
    }

    public String getEventDay() {
        return eventDay;
    }

    public String getEventMon() {
        return eventMon;
    }

    public String getEventYear() {
        return eventYear;
    }
       private String dayOf(int value)
       { String theDay="";
       switch(value)
       {
           case 1:
           {
               theDay="Mon";
               break;
           }
             case 2:
           {
               theDay="Tue";
               break;
           }
            case 3:
           {
               theDay="Wed";
               break;
           }
              case 4:
           {
               theDay="Thu";
               break;
           }
              
             case 5:
           {
               theDay="Fri";
               break;
           }
               case 6:
           {
               theDay="Sat";
               break;
           }
                 case 0:
           {
               theDay="Sun";
               break;
           }
       }
           return theDay;
       
       }

    public String getEventDayText() {
        return eventDayText;
    }

    public void setEventDayText(String eventDayText) {
        this.eventDayText = eventDayText;
    }

    public String getEventMonText() {
        return eventMonText;
    }

    public void setEventMonText(String eventMonText) {
        this.eventMonText = eventMonText;
    }
       
         private String monthOf(int value)
       { String theMonth="";
       switch(value)
       {
           case 1:
           {
               theMonth="Jan";
               break;
           }
             case 2:
           {
               theMonth="Feb";
               break;
           }
            case 3:
           {
               theMonth="Mar";
               break;
           }
              case 4:
           {
               theMonth="Apr";
               break;
           }
              
             case 5:
           {
               theMonth="May";
               break;
           }
               case 6:
           {
               theMonth="Jun";
               break;
           }
                 case 7:
           {
               theMonth="Jul";
               break;
           }
                case 8:
           {
               theMonth="Aug";
               break;
           }
               case 9:
           {
               theMonth="Sep";
               break;
           }
                 case 10:
           {
               theMonth="Oct";
               break;
           }              
                          case 11:
           {
               theMonth="Nov";
               break;
           }
                 case 12:
           {
               theMonth="Dec";
               break;
           }       
       }
           return theMonth;
       
       }     
       
}
